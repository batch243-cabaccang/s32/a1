const server = require("http");
const port = 8000;

server
  .createServer((req, res) => {
    if (req.url == "/" && req.method == "GET") {
      res.writeHead(200, { "Content-Type": "text/plan" });
      res.write("Welcome to Booking System");
      res.end();
      return;
    }
    if (req.url == "/profile" && req.method == "GET") {
      res.writeHead(200, { "Content-Type": "text/plan" });
      res.write("Welcome to Your Profile");
      res.end();
      return;
    }
    if (req.url == "/courses" && req.method == "GET") {
      res.writeHead(200, { "Content-Type": "text/plan" });
      res.write("Here's Our Courses Available");
      res.end();
      return;
    }
    if (req.url == "/addcourse" && req.method == "POST") {
      res.writeHead(200, { "Content-Type": "text/plan" });
      res.write("Add Course to our Resources");
      res.end();
      return;
    }
  })
  .listen(port);

console.log(`Listeinig to Port ${port}`);
